import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class PersonsService {
  private BASE_URL: string = 'https://swapi.dev/api/';
  personsChanged = new Subject<string[]>();
  persons: string[] = [];

  addPerson(name: string) {
    this.persons.push(name);
    this.personsChanged.next(this.persons);
  }

  removePerson(name: string) {
    this.persons = this.persons.filter((person) => {
      return person !== name;
    });
    this.personsChanged.next(this.persons);
  }

  constructor(private http: HttpClient) {}

  fetchPersons() {
    this.http
      .get<any>(`${this.BASE_URL}people`)
      .pipe(
        map((resData) => {
          return resData.results.map((character) => character.name);
        })
      )
      .subscribe((transformedData) => {
        this.personsChanged.next(transformedData);
      });
  }
}
